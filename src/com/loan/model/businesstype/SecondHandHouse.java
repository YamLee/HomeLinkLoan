package com.loan.model.businesstype;

/**
 * Created by LiYan on 2015/5/30.
 */
public class SecondHandHouse implements IBusinessType {
    @Override
    public int getTypeNum() {
        return 3;
    }

    @Override
    public String getTypeName() {
        return "二手房、非法院拍卖";
    }

    @Override
    public String getNetParamValue() {
        return "297edff83cf6b763013cf634a2f80006";
    }
}
