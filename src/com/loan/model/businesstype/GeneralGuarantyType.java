package com.loan.model.businesstype;

/**
 * Created by LiYan on 2015/5/30.
 */
public class GeneralGuarantyType implements IBusinessType {
    @Override
    public int getTypeNum() {
        return 13;
    }

    @Override
    public String getTypeName() {
        return "一般抵押权登记、在建工程抵押转现房抵押";
    }

    @Override
    public String getNetParamValue() {
        return "297edff83cf6b763013cf64554e40010";
    }

}
