package com.loan.model.businesstype;

/**
 * Created by LiYan on 2015/5/30.
 */
public class NewHouseType implements IBusinessType {
    @Override
    public int getTypeNum() {
        return 2;
    }

    @Override
    public String getTypeName() {
        return "一手房";
    }

    @Override
    public String getNetParamValue() {
        return "297edff83cf6b763013cf63363c70005";
    }
}
