package com.loan.model.businesstype;

/**
 * Created by LiYan on 2015/5/30.
 */
public interface IBusinessType {
    int getTypeNum();
    String getTypeName();
    String getNetParamValue();
}
