package com.loan.model.businesstype;

/**
 * Created by LiYan on 2015/5/30.
 */
public class PreBuyHouseCheckType implements IBusinessType {
    @Override
    public int getTypeNum() {
        return 15;
    }

    @Override
    public String getTypeName() {
        return "预购商品房预告登记";
    }

    @Override
    public String getNetParamValue() {
        return "297edff83cf6b763013cf64deedf0012";
    }
}
