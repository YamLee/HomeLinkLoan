package com.loan.model.businesstype;

/**
 * Created by LiYan on 2015/5/30.
 */
public class PreBuyHouseGuarantyCheckType implements IBusinessType {
    @Override
    public int getTypeNum() {
        return 16;
    }

    @Override
    public String getTypeName() {
        return "预购商品房抵押权预告登记";
    }

    @Override
    public String getNetParamValue() {
        return "297edff83cf6b763013cf64edcd50013";
    }
}
