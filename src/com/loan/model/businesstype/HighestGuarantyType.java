package com.loan.model.businesstype;

/**
 * Created by LiYan on 2015/5/30.
 */
public class HighestGuarantyType implements IBusinessType {
    @Override
    public int getTypeNum() {
        return 14;
    }

    @Override
    public String getTypeName() {
        return "最高额抵押权登记";
    }

    @Override
    public String getNetParamValue() {
        return "297edff83cf6b763013cf6463acf0011";
    }
}
