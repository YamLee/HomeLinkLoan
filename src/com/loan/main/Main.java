package com.loan.main;

import com.loan.constants.Url;
import com.loan.model.FormBean;
import com.loan.model.businesstype.GeneralGuarantyType;
import com.loan.model.businesstype.IBusinessType;
import org.apache.http.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import sun.nio.cs.ext.GBK;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by LiYan on 2015/5/30.
 */
public class Main {
    private static String cookie = "";
    //    private Ma
    public static void main(String[] args) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            goFirst(httpclient);
            goSecond(httpclient);
            Thread.sleep(21 * 1000);
            goThird(httpclient);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void goFirst(CloseableHttpClient httpclient) throws IOException {
        HttpGet httpGet = new HttpGet(getFirstUri());
        CloseableHttpResponse response1 = httpclient.execute(httpGet);

        try {

            System.out.println(response1.getStatusLine());
            HttpEntity entity1 = response1.getEntity();
            cookie = response1.getFirstHeader("Set-Cookie").getValue();
            cookie = cookie.replace("; path=/","");
            System.out.println("cookie---"+cookie);
            Header[] headers = response1.getAllHeaders();
            for(int i=0;i<headers.length ; i++){
                System.out.println(headers[i].getName()+"---->"+headers[i].getValue());
            }
//                EntityUtils.consume(entity1);
//            String content = EntityUtils.toString(entity1);
//            System.out.println("content 1-"+content);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            response1.close();
        }
    }

    private static void goSecond(CloseableHttpClient httpclient) throws IOException {
        IBusinessType businessType = new GeneralGuarantyType();
        HttpGet httpGet = new HttpGet(getSecodUri(businessType));
        httpGet.setHeader("Cookie", cookie);
        CloseableHttpResponse response1 = httpclient.execute(httpGet);

        try {

            System.out.println(response1.getStatusLine());
            HttpEntity entity1 = response1.getEntity();
//                EntityUtils.consume(entity1);
            String content = EntityUtils.toString(entity1);
            System.out.println("content 2-"+content);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            response1.close();
        }
    }


    private static void goThird(CloseableHttpClient httpclient) throws IOException {
        IBusinessType businessType = new GeneralGuarantyType();
        HttpGet httpGet = new HttpGet(getThirdUri(businessType));

        httpGet.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36");
        // 以下这条如果不加会发现无论你设置Accept-Charset为gbk还是utf-8，他都会默认返回gb2312（本例针对google.cn来说）
        // 用逗号分隔显示可以同时接受多种编码
        httpGet.setHeader("Accept-Language", "zh-cn,zh;q=0.5");
        httpGet.setHeader("Accept-Charset", "GB2312,utf-8;q=0.7,*;q=0.7");
        httpGet.setHeader("Host", "fwjy.bjchy.gov.cn:81");
        httpGet.setHeader("Connection", "keep-alive");
        httpGet.setHeader("Cache-Control", "max-age=0");
        httpGet.setHeader("Cookie", cookie);

        CloseableHttpResponse response1 = httpclient.execute(httpGet);
        InputStream inputStream = null;
        try {

            System.out.println(response1.getStatusLine());
            Header[] headers = response1.getAllHeaders();
            for(int i=0;i<headers.length ; i++){
                System.out.println(headers[i].getName()+"---->"+headers[i].getValue());
            }
            HttpEntity entity1 = response1.getEntity();
//                EntityUtils.consume(entity1);
//            String content = EntityUtils.toString(entity1);
             inputStream = entity1.getContent();
            if(null != inputStream){
                byte[] bytes = new byte[1024*10*10];
                inputStream.read(bytes);
                String str = new String(bytes, "GBK");
                System.out.println("content 3-content:"+ str);
            }
            System.out.println("content 3-content length:"+ entity1.getContentLength());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            response1.close();
            inputStream.close();
        }
    }

    private static void goFourth(CloseableHttpClient httpclient) throws IOException {
        FormBean formBean = new FormBean();
        formBean.appointDate = "";

        IBusinessType businessType = new GeneralGuarantyType();
        HttpGet httpGet = new HttpGet(getFourthUri(businessType,formBean));
        CloseableHttpResponse response1 = httpclient.execute(httpGet);

        try {

            System.out.println(response1.getStatusLine());
            HttpEntity entity1 = response1.getEntity();
//                EntityUtils.consume(entity1);
            String content = EntityUtils.toString(entity1);
            System.out.println("content-"+content);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            response1.close();
        }
    }

    private static URI getFirstUri() {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("m","add"));
        return getUri(params);
    }

    private static URI getSecodUri(IBusinessType businessType){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("token",""));
        params.add(new BasicNameValuePair("m","edit"));
        params.add(new BasicNameValuePair("result",""));
        params.add(new BasicNameValuePair("server_id",""));
        params.add(new BasicNameValuePair("service_date",""));
        params.add(new BasicNameValuePair("service_names",""));
        params.add(new BasicNameValuePair("service_card",""));
        params.add(new BasicNameValuePair("service_ne",businessType.getNetParamValue()));
        params.add(new BasicNameValuePair("p_Appointment_temp1",""));
        params.add(new BasicNameValuePair("p_Appointment_server_id",businessType.getNetParamValue()));
        params.add(new BasicNameValuePair("j_captcha_response","zwx7"));

        return getUri(params);

    }

    private static URI getThirdUri(IBusinessType businessType){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("token",""));
        params.add(new BasicNameValuePair("m","showAddPage"));
        params.add(new BasicNameValuePair("p_Appointment_id",businessType.getNetParamValue()));
        params.add(new BasicNameValuePair("serviceid",businessType.getNetParamValue()));
        return getUri(params);
    }

    private static URI getFourthUri(IBusinessType businessType,FormBean form){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("token",""));
        params.add(new BasicNameValuePair("m","saveAdd"));
        params.add(new BasicNameValuePair("info","0"));
        params.add(new BasicNameValuePair("result",""));
        params.add(new BasicNameValuePair("server_id",businessType.getNetParamValue()));
        params.add(new BasicNameValuePair("service_date",""));
        params.add(new BasicNameValuePair("service_names",""));
        params.add(new BasicNameValuePair("service_card",""));
        params.add(new BasicNameValuePair("service_ne",""));
        params.add(new BasicNameValuePair("p_Appointment_temp1",""));
        params.add(new BasicNameValuePair("yybh",form.businessNum));
        params.add(new BasicNameValuePair("p_Appointment_app_date",form.appointDate));
        params.add(new BasicNameValuePair("p_Appointment_app_1",form.businessNum));
        params.add(new BasicNameValuePair("p_Appointment_app_16",form.houseOwnNum));
        params.add(new BasicNameValuePair("sx",form.isGurantyAPerson));
        params.add(new BasicNameValuePair("p_Appointment_app_2",form.isGurantyAPerson));
        params.add(new BasicNameValuePair("p_Appointment_app_3",form.gurantyName));
        params.add(new BasicNameValuePair("p_Appointment_app_4","yyzzh"));
        params.add(new BasicNameValuePair("p_Appointment_app_5",form.gurantyNum));
        params.add(new BasicNameValuePair("p_Appointment_app_6",form.gurantyAgentPeopleName));
        params.add(new BasicNameValuePair("p_Appointment_app_7","sfz"));
        params.add(new BasicNameValuePair("p_Appointment_app_8",form.gurantyAgentPeopleNum));
        params.add(new BasicNameValuePair("s","1"));
        params.add(new BasicNameValuePair("p_Appointment_app_9","1"));
        params.add(new BasicNameValuePair("p_Appointment_app_10",form.gurantedPeopleName));
        params.add(new BasicNameValuePair("p_Appointment_app_11","sfz"));
        params.add(new BasicNameValuePair("p_Appointment_app_12",form.gurantedPeopleNum));
        params.add(new BasicNameValuePair("p_Appointment_app_13",""));
        params.add(new BasicNameValuePair("p_Appointment_app_14",""));
        params.add(new BasicNameValuePair("p_Appointment_app_15",""));
        params.add(new BasicNameValuePair("p_Appointment_phonenumber",form.appointPeoplePhoneNum));
        params.add(new BasicNameValuePair("p_Appointment_testmsgtemp",form.passwd));
        params.add(new BasicNameValuePair("p_Appointment_testmsg","49BA59ABBE56E057"));
        params.add(new BasicNameValuePair("p_Appointment_testmsgtempnext",form.confirmPasswd));
        params.add(new BasicNameValuePair("j_captcha_response","阴厘"));
        return getUri(params);
    }

    private static URIBuilder getBaseUriBuilder() throws URISyntaxException {
        return new URIBuilder().setScheme("http")
                .setHost("fwjy.bjchy.gov.cn:81")
                .setPath("/yyfh.appointment.do");

    }

    private static URI getUri(List<NameValuePair> params) {
        URI uri = null;
        try {
            uri  = getBaseUriBuilder().setParameters(params).build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return uri;
    }


}
